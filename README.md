# Django GoCardless

This is a simplistic library for validating and creating payments
and subscriptions for GoCardless in Django

## Usage

This package exposes a set of API endpoints for the GoCardless API.

Documentation to come.

## Warning

This is not production ready in any way and is in active development.
Breaking changes are likely.

## Testing and Development

Requirements for development are in `dev-requirements.txt`.

This project uses pytest. You can test by running:

    pytest .
