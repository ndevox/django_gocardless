-r requirements.txt
coverage
factory_boy
flake8
pytest
pytest-cov
pytest-django
tox
