from django.apps import AppConfig


class GocardlessPaymentsConfig(AppConfig):
    name = 'django_gocardless'
