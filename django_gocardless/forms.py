"""
Forms for the GoCardless system.
"""

import datetime

from django import forms
from django.db import transaction

from django_gocardless import models, utils


class BaseGoCardlessForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = utils.get_client()

    def clean(self):
        """ Ensure the mandate given is the users mandate. """
        cleaned_data = super().clean()

        if cleaned_data['user'].gocardlessmandate.pk != cleaned_data['mandate'].pk:
            raise forms.ValidationError('Invalid mandate.')

        return cleaned_data


class GoCardlessPaymentForm(BaseGoCardlessForm):
    """ Form for creating GoCardless payments. """

    @transaction.atomic
    def save(self, *args, **kwargs) -> models.GoCardlessPayment:
        """ Create and submit a payment to GoCardless. """
        payment = super().save(*args, **kwargs)

        gc_payment = self.client.payments.create(
            params={
                'amount': payment.amount,
                'currency': 'GBP',
                'links': {
                    'mandate': payment.mandate.mandate_id
                }
            },
            headers={
                'Idempotency-Key': payment.idempotent_key()
            }
        )

        payment.payment_id = gc_payment.id

        payment.save()

        return payment

    class Meta:
        model = models.GoCardlessPayment
        fields = '__all__'


class GoCardlessSubscriptionForm(BaseGoCardlessForm):
    """ Form for creating GoCardless subscriptions. """

    @transaction.atomic
    def save(self, *args, **kwargs) -> models.GoCardlessSubscription:
        """ Create and submit a subscription to GoCardless. """
        subscription = super().save(*args, **kwargs)

        gc_subscription = self.client.subscriptions.create(
            params={
                'amount': subscription.amount,
                'currency': 'GBP',
                'interval_unit': 'monthly',
                'day_of_month': str(datetime.date.today().day),
                'links': {
                    'mandate': subscription.mandate.mandate_id
                }
            },
            headers={
                'Idempotency-Key': subscription.idempotent_key()
            }
        )

        subscription.subscription_id = gc_subscription.id
        subscription.active = True
        subscription.save()

        return subscription

    class Meta:
        model = models.GoCardlessSubscription
        fields = '__all__'
