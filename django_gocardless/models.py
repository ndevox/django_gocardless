"""
Models specific for payments related to Go Cardless and Direct Debits.

This model system is a bit more complex due to the webhook callbacks we get
from GoCardless requiring extra actions on each class.
"""
import uuid

from django.conf.global_settings import AUTH_USER_MODEL
from django.contrib.postgres.fields import JSONField
from django.db import models


class GoCardlessEvent(models.Model):
    """ Simple model to hold onto event data. This may be excessive. """
    metadata = JSONField()


class GoCardlessBaseModel(models.Model):
    """
    Provide a generic basis for dealing with webhook actions.

    We provide an event_metadata field for storing all events.

    We also provide some basic webhook methods. These are the ones we always expect
    and are:

      - cancelled
      - failed
      - confirmed
      - submitted
    """
    id_key = None
    id_field = None

    is_failed = models.NullBooleanField()
    is_cancelled = models.NullBooleanField()

    event_metadata = models.ManyToManyField(GoCardlessEvent, blank=True)

    class Meta:
        abstract = True

    @classmethod
    def get_object(cls, event: dict) -> models.Model:
        """ Get the object related to this event and return it. """
        obj_id = {cls.id_field: event['links'][cls.id_key]}
        return cls.objects.get(**obj_id)

    @classmethod
    def process_event(cls, event: dict):
        """ Process and save the event. """
        obj = cls.get_object(event)

        getattr(cls, event['action'])(event, obj)
        obj.event_metadata.create(metadata=event)
        obj.save()

    @classmethod
    def cancelled(cls, _event: dict, obj: models.Model):
        """ Cancel an object. """
        obj.active = False
        obj.is_cancelled = True
        obj.is_failed = False

    @classmethod
    def failed(cls, _event: dict, obj: models.Model):
        """ Fail an object. """
        obj.active = False
        obj.is_cancelled = False
        obj.is_failed = True

    @classmethod
    def confirmed(cls, event: dict, obj: models.Model):
        raise NotImplementedError

    @classmethod
    def submitted(cls, event: dict, obj: models.Model):
        raise NotImplementedError


class GoCardlessMandate(GoCardlessBaseModel):
    """
    The main model handling customer DD mandates.
    """
    user = models.OneToOneField(AUTH_USER_MODEL, on_delete=models.CASCADE)

    mandate_id = models.CharField(max_length=128, null=True, blank=True, unique=True)
    active = models.BooleanField(default=False)

    # Stuff specific to setting up the mandate.
    redirect_flow_id = models.CharField(max_length=64, null=True, blank=True, unique=True)
    _session_token = models.CharField(max_length=64, null=True, blank=True, unique=True)

    id_key = 'mandate'
    id_field = 'mandate_id'

    def session_token(self) -> str:
        """
        Generate a session token.

        If we already have one hold onto it and return that.

        :return: the session token.
        :rtype: str
        """
        if self._session_token:
            return self._session_token
        self._session_token = str(uuid.uuid4())
        self.save()
        return self._session_token

    @classmethod
    def confirmed(cls, event: dict, obj: models.Model):
        """ Done through our system so we don't care. """
        pass

    @classmethod
    def submitted(cls, event: dict, obj: models.Model):
        """ Done through our system so we don't care. """
        pass


class GoCardlessBasePayment(GoCardlessBaseModel):
    """
    A base model handling payment information.
    """
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    mandate = models.ForeignKey(GoCardlessMandate, on_delete=models.PROTECT)

    amount = models.IntegerField()
    _idempotent_key = models.CharField(max_length=64, null=True, blank=True, unique=True)

    def idempotent_key(self) -> str:
        """
        Generate an idempotent key.

        If we already have one hold onto it and return that.

        :return: the idemptoent key.
        :rtype: str
        """
        if self._idempotent_key:
            return self._idempotent_key
        self._idempotent_key = str(uuid.uuid4())
        self.save()
        return self._idempotent_key

    class Meta:
        abstract = True


class GoCardlessPayment(GoCardlessBasePayment):
    """
    A model for handling actual payments.
    """
    payment_id = models.CharField(max_length=64, unique=True, blank=True)
    success = models.BooleanField(default=False)

    id_field = 'payment_id'
    id_key = 'payment'

    @classmethod
    def confirmed(cls, event: dict, obj: models.Model):
        """ Mark as success. """
        obj.success = True

    @classmethod
    def submitted(cls, event: dict, obj: models.Model):
        """ We should already know this by submitting through our system. """
        pass


class GoCardlessSubscription(GoCardlessBasePayment):
    """
    A model for handling subscriptions for payments.
    """
    subscription_id = models.CharField(max_length=64, unique=True, blank=True)
    active = models.BooleanField(default=False)

    payments = models.ManyToManyField(GoCardlessPayment, blank=True)

    id_field = 'subscription_id'
    id_key = 'subscription'

    @classmethod
    def confirmed(cls, event: dict, obj):
        """ Subscription confirmed should make it active. """
        obj.active = True
        obj.is_failed = False
        obj.is_cancelled = False

    @classmethod
    def submitted(cls, event: dict, obj: models.Model):
        """ Done through our system so we don't care about this hook. """
        pass

    @classmethod
    def payment_created(cls, event: dict, obj: models.Model):
        """ Need to create a payment on our system if this is the case. """
        obj.payments.get_or_create(
            payment_id=event['links'][GoCardlessPayment.id_field],
            amount=obj.amount,
            user=obj.user,
            mandate=obj.mandate
        )
