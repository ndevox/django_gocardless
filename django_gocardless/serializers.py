"""
Serializers for GoCardless Models.
"""
from rest_framework import serializers

from django_gocardless import models


class GoCardlessMandateSerializer(serializers.ModelSerializer):
    """ Serializer for GoCardless Mandates. """
    class Meta:
        model = models.GoCardlessMandate
        fields = '__all__'


class BaseGoCardlessSerializer(serializers.ModelSerializer):
    """ Base serializer for GoCardless payments/subscriptions. """
    mandate = GoCardlessMandateSerializer()


class GoCardlessPaymentSerializer(BaseGoCardlessSerializer):
    """ Basse serializer for GoCardless Payments. """
    class Meta:
        model = models.GoCardlessPayment
        fields = '__all__'


class GoCardlessSubscriptionSerializer(BaseGoCardlessSerializer):
    """ Basse serializer for GoCardless Subscriptions. """
    class Meta:
        model = models.GoCardlessSubscription
        fields = '__all__'
