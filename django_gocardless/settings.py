"""
THESE SETTINGS ARE FOR TESTING ONLY.
"""

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'dummy'


# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django_gocardless'
]

ROOT_URLCONF = 'django_gocardless.urls'

WSGI_APPLICATION = 'django_gocardless.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': 'postgres',
        'NAME': 'django_gocardless',
        'USER': 'django_gocardless',
        'PASSWORD': 'django_gocardless',
    }
}

# Django GoCardless settings
GO_CARDLESS_KEY = 'dummy'
GO_CARDLESS_WEBHOOK_SECRET = 'dummy'
GO_CARDLESS_DESCRIPTION = 'test'
