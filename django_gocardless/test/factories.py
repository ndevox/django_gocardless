""" Factories for the GoCardless payments system. """
import uuid

import factory
from django.contrib.auth.models import User

from django_gocardless import models


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.sequence(lambda x: 'TestUser{}'.format(x))
    password = 'test'


class GoCardlessMandateFactory(factory.DjangoModelFactory):
    """ Factory for GoCardless Mandates """
    class Meta:
        model = models.GoCardlessMandate

    user = factory.SubFactory(UserFactory)
    mandate_id = factory.LazyFunction(lambda: str(uuid.uuid4()))


class GoCardlessBasePaymentFactory(factory.DjangoModelFactory):
    """ Factory for base GoCardless payments """
    class Meta:
        model = models.GoCardlessBasePayment
    mandate = factory.SubFactory(GoCardlessMandateFactory)
    amount = 2000
    user = factory.SubFactory(UserFactory)


class GoCardlessPaymentFactory(GoCardlessBasePaymentFactory):
    """ Factory for GoCardless Payments """
    class Meta:
        model = models.GoCardlessPayment
    payment_id = factory.LazyFunction(lambda: str(uuid.uuid4()))


class GoCardlessSubscriptionFactory(GoCardlessBasePaymentFactory):
    """ Factory for GoCardless Subscriptions """
    class Meta:
        model = models.GoCardlessSubscription
    subscription_id = factory.LazyFunction(lambda: str(uuid.uuid4()))
