from unittest import mock

from django_gocardless import forms, models
from django_gocardless.test import factories

import pytest


@pytest.fixture
def test_form_data():
    mandate = factories.GoCardlessMandateFactory()
    return {
        'mandate': mandate.pk,
        'amount': 2000,
        'user': mandate.user.pk
    }


@pytest.mark.django_db
@mock.patch('django_gocardless.utils.get_client')
def test_subscription_execute(mock_client, test_form_data):

    subs_ret = mock.MagicMock()
    subs_ret.id = 'test_id'

    mock_sub = mock.MagicMock()
    mock_sub.subscriptions.create.return_value = subs_ret

    mock_client.return_value = mock_sub

    form = forms.GoCardlessSubscriptionForm(
        test_form_data
    )

    assert form.is_valid()
    assert isinstance(form.save(), models.GoCardlessSubscription)


@pytest.mark.django_db
@mock.patch('django_gocardless.utils.get_client')
def test_payment_execute(mock_client, test_form_data):

    pay_ret = mock.MagicMock()
    pay_ret.id = 'test_id'

    mock_pay = mock.MagicMock()
    mock_pay.payments.create.return_value = pay_ret

    mock_client.return_value = mock_pay

    form = forms.GoCardlessPaymentForm(
        test_form_data
    )

    assert form.is_valid()
    assert isinstance(form.save(), models.GoCardlessPayment)


@pytest.mark.django_db
def test_invalid_gc_form(test_form_data):

    test_form_data['mandate'] = factories.GoCardlessMandateFactory().pk

    form = forms.GoCardlessSubscriptionForm(
        test_form_data
    )

    assert not form.is_valid()
