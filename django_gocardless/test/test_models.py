""" Test cases for the Go Cardless Models system. """
from django.test import TestCase

from django_gocardless import models
from django_gocardless.test import factories


class GoCardlessMandateTestCase(TestCase):
    """ GoCardlessMandate Test Cases """

    def setUp(self):
        self.mandate: models.GoCardlessMandate = factories.GoCardlessMandateFactory()
        self.event = {'links': {'mandate': self.mandate.mandate_id}}

    def test_session_token(self):
        """ Ensure session_token is generated and then kept. """
        assert not self.mandate._session_token
        token = self.mandate.session_token()
        assert self.mandate._session_token
        assert self.mandate._session_token == token
        # We need to make sure we get back the same token every time.
        assert self.mandate.session_token() == token

    def test_confirmed(self):
        """ Ensure confirm doesn't error. """
        self.mandate.confirmed(self.event, self.mandate)

    def test_submitted(self):
        """ Ensure submitted doesn't error. """
        self.mandate.submitted(self.event, self.mandate)

    def test_cancelled(self):
        """ Ensure cancelled cancels the object. """
        self.mandate.active = True
        self.mandate.save()
        self.mandate.cancelled(self.event, self.mandate)

        assert not self.mandate.active
        assert self.mandate.is_cancelled
        assert not self.mandate.is_failed

    def test_failed(self):
        """ Ensure failed fails the object. """
        self.mandate.active = True
        self.mandate.save()
        self.mandate.failed(self.event, self.mandate)

        assert not self.mandate.active
        assert not self.mandate.is_cancelled
        assert self.mandate.is_failed

    def test_get_object(self):
        """ Ensure get object gets the actual object. """
        assert self.mandate.get_object(self.event) == self.mandate

    def test_process_event(self):
        """ Ensure process_event adds the event to the metadata. """
        self.event['action'] = 'submitted'
        assert not self.mandate.event_metadata.count()
        self.mandate.process_event(self.event)
        assert self.mandate.event_metadata.count() == 1


class GoCardlessPaymentsTestCase(TestCase):
    """ Test cases for GoCardless Payments. """
    def setUp(self):
        self.payment: models.GoCardlessPayment = factories.GoCardlessPaymentFactory()
        self.event = {'links': {'payment': self.payment.payment_id}}

    def test_confirmed(self):
        """ Ensure payment is a success on confirmation. """
        assert not self.payment.success
        self.payment.confirmed(self.event, self.payment)
        assert self.payment.success
        assert not self.payment.is_failed
        assert not self.payment.is_cancelled

    def test_submitted(self):
        """ Ensure submitted doesn't error. """
        self.payment.submitted(self.event, self.payment)

    def test_idempotent_key(self):
        """ Ensure we generate then keep the idempotent_key. """
        assert not self.payment._idempotent_key
        key = self.payment.idempotent_key()
        assert self.payment._idempotent_key
        assert self.payment._idempotent_key == key
        # We need to make sure we get back the same key every time.
        assert self.payment.idempotent_key() == key

    def test_get_object(self):
        """ Ensure get object gets the actual object. """
        assert self.payment.get_object(self.event) == self.payment

    def test_process_event(self):
        """ Ensure process_event adds the event to the metadata. """
        self.event['action'] = 'submitted'
        assert not self.payment.event_metadata.count()
        self.payment.process_event(self.event)
        assert self.payment.event_metadata.count() == 1


class GoCardlessSubscriptionTestCase(TestCase):
    """ Test cases for Go Cardless subscriptions. """
    def setUp(self):
        self.subscription: models.GoCardlessSubscription = factories.GoCardlessSubscriptionFactory()
        self.event = {'links': {'subscription': self.subscription.subscription_id}}

    def test_confirmed(self):
        """ Ensure subscription is active on confirmation. """
        assert not self.subscription.active
        self.subscription.confirmed(self.event, self.subscription)
        assert self.subscription.active
        assert not self.subscription.is_failed
        assert not self.subscription.is_cancelled

    def test_submitted(self):
        """ Ensure submitted doesn't error. """
        self.subscription.submitted(self.event, self.subscription)

    def test_payment_created(self):
        assert not self.subscription.payments.count()
        self.event['links'][models.GoCardlessPayment.id_field] = 'test_id'
        self.subscription.payment_created(self.event, self.subscription)
        assert self.subscription.payments.count() == 1
        assert self.subscription.payments.get().payment_id == 'test_id'

    def test_idempotent_key(self):
        """ Ensure we generate then keep the idempotent_key. """
        assert not self.subscription._idempotent_key
        key = self.subscription.idempotent_key()
        assert self.subscription._idempotent_key
        assert self.subscription._idempotent_key == key
        # We need to make sure we get back the same key every time.
        assert self.subscription.idempotent_key() == key

    def test_get_object(self):
        """ Ensure get object gets the actual object. """
        assert self.subscription.get_object(self.event) == self.subscription

    def test_process_event(self):
        """ Ensure process_event adds the event to the metadata. """
        self.event['action'] = 'submitted'
        assert not self.subscription.event_metadata.count()
        self.subscription.process_event(self.event)
        assert self.subscription.event_metadata.count() == 1
