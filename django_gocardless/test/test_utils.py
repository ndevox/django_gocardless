""" Test cases for the GoCardless Utils. """
import hashlib
import hmac
from unittest import mock

from django.conf import settings
from django_gocardless import utils


@mock.patch('gocardless_pro.Client')
def test_get_client(mock_client):
    """ Ensure we call the gocardless Client API. """
    utils.get_client()
    assert mock_client.call_count == 1


def test_valid_signature():
    """ Ensure we validate the signature appropriately. """
    secret = bytes(settings.GO_CARDLESS_WEBHOOK_SECRET, 'utf-8')
    body = b'test signature'
    sig = hmac.new(
        secret, body, hashlib.sha256
    ).hexdigest()

    assert utils.is_valid_signature(body, sig)
