import json
import uuid
from unittest import mock

from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
from rest_framework.test import APIClient

from django_gocardless import models
from django_gocardless.test import factories


WEBHOOK_CLIENT = APIClient(HTTP_WEBHOOK_SIGNATURE='')
WEBHOOK_EVENT = {"events": [{"resource_type": "mandates", "action": "failed"}]}


class MandateViewSetTestCase(TestCase):
    def setUp(self):
        self.mandate: models.GoCardlessMandate = factories.GoCardlessMandateFactory()
        self.mandate.session_token()
        self.mandate.redirect_flow_id = uuid.uuid4()
        self.mandate.save()

        self.patched_client = mock.patch('gocardless_pro.Client')
        self.mock_client = self.patched_client.start()
        self.addCleanup(self.patched_client.stop)

        self.mock_ret_client = mock.MagicMock()
        self.mock_client.return_value = self.mock_ret_client

        self.client = APIClient()
        self.client.force_authenticate(self.mandate.user)

    def test_mandate_create_view(self):
        redirect_ret = mock.MagicMock()
        redirect_ret.id = self.mandate.redirect_flow_id
        redirect_ret.redirect_url = 'test_url'
        self.mock_ret_client.redirect_flows.create.return_value = redirect_ret

        resp = self.client.post('/mandates/')

        assert resp.status_code == 200

        assert resp.json() == {'url': 'test_url'}

    def test_fail_create_view(self):
        self.mandate.active = True
        self.mandate.save()

        resp = self.client.post('/mandates/')

        assert resp.status_code == 409
        assert resp.json() == 'Mandate already exists'

    def test_mandate_success_view(self):
        mandate_id = str(uuid.uuid4())

        redirect_ret = mock.MagicMock()
        redirect_ret.links.mandate = mandate_id
        redirect_ret.confirmation_url = 'test_url'
        self.mock_ret_client.redirect_flows.complete.return_value = redirect_ret

        resp = self.client.get(
            '/mandates/{}/success/'.format(self.mandate.pk),
            {'redirect_flow_id': self.mandate.redirect_flow_id}
        )

        assert resp.status_code == 200
        assert resp.json() == {'url': 'test_url'}

        self.mandate.refresh_from_db()

        assert self.mandate.active
        assert self.mandate.mandate_id == mandate_id

    def test_mandate_failed_success_view(self):
        resp = self.client.get(
            '/mandates/{}/success/'.format(self.mandate.pk)
        )

        assert resp.status_code == 400


class BaseGoCardlessViewSetTestCase(TestCase):
    form = ''

    def setUp(self):
        self.mandate: models.GoCardlessMandate = factories.GoCardlessMandateFactory()
        self.user = self.mandate.user

        self.client = APIClient()
        self.client.force_authenticate(self.user)

        self.mock_form_ret = mock.MagicMock()

        self.patched_form = mock.patch(self.form)
        self.mock_form = self.patched_form.start()
        self.addCleanup(self.patched_form.stop)
        self.mock_form.return_value = self.mock_form_ret


class GoCardlessPaymentViewSetTestCase(BaseGoCardlessViewSetTestCase):
    form = 'django_gocardless.views.GoCardlessPaymentViewSet.form'

    def test_create(self):
        self.mock_form_ret.is_valid.return_value = True

        resp = self.client.post('/payments/')

        assert resp.status_code == 201

    def test_failed_create(self):
        self.mock_form_ret.is_valid.return_value = False

        self.mock_form_ret.errors.as_json.return_value = 'test'

        resp = self.client.post('/payments/')

        assert resp.status_code == 400
        assert resp.json() == 'test'

    def test_queryset(self):

        user_payment = factories.GoCardlessPaymentFactory(
            user=self.user,
            mandate=self.mandate
        )
        factories.GoCardlessPaymentFactory()

        resp = self.client.get('/payments/')

        assert resp.status_code == 200
        assert len(resp.json()) == 1
        assert resp.json()[0]['id'] == user_payment.pk


class GoCardlessSubscriptionViewSetTestCase(BaseGoCardlessViewSetTestCase):
    form = 'django_gocardless.views.GoCardlessSubscriptionViewSet.form'

    def test_create(self):
        self.mock_form_ret.is_valid.return_value = True

        resp = self.client.post('/subscriptions/')

        assert resp.status_code == 201

    def test_failed_create(self):
        self.mock_form_ret.is_valid.return_value = False

        self.mock_form_ret.errors.as_json.return_value = 'test'

        resp = self.client.post('/subscriptions/')

        assert resp.status_code == 400
        assert resp.json() == 'test'

    def test_queryset(self):

        user_sub = factories.GoCardlessSubscriptionFactory(
            user=self.user,
            mandate=self.mandate
        )
        factories.GoCardlessSubscriptionFactory()

        resp = self.client.get('/subscriptions/')

        assert resp.status_code == 200
        assert len(resp.json()) == 1
        assert resp.json()[0]['id'] == user_sub.pk


@mock.patch('django_gocardless.models.GoCardlessMandate.process_event')
@mock.patch('django_gocardless.utils.is_valid_signature')
def test_webhook(mock_sig: mock.MagicMock, mock_event: mock.MagicMock):
    mock_sig.return_value = True
    mock_event.return_value = True
    assert WEBHOOK_CLIENT.post(
        '/webhook/',
        json.dumps(WEBHOOK_EVENT),
        content_type='application/json'
    ).status_code == 200


@mock.patch('django_gocardless.utils.is_valid_signature')
def test_webhook_invalid_sig(mock_sig: mock.MagicMock):
    mock_sig.return_value = False
    assert WEBHOOK_CLIENT.post(
        '/webhook/',
        json.dumps(WEBHOOK_EVENT),
        content_type='application/json'
    ).status_code == 498


@mock.patch('django_gocardless.models.GoCardlessMandate.process_event')
@mock.patch('django_gocardless.utils.is_valid_signature')
def test_webhook_attr_error(mock_sig: mock.MagicMock, mock_event: mock.MagicMock):
    mock_sig.return_value = True
    mock_event.side_effect = AttributeError()
    assert WEBHOOK_CLIENT.post(
        '/webhook/',
        json.dumps(WEBHOOK_EVENT),
        content_type='application/json'
    ).status_code == 200


@mock.patch('django_gocardless.models.GoCardlessMandate.process_event')
@mock.patch('django_gocardless.utils.is_valid_signature')
def test_webhook_not_exist(mock_sig: mock.MagicMock, mock_event: mock.MagicMock):
    mock_sig.return_value = True
    mock_event.side_effect = ObjectDoesNotExist()
    assert WEBHOOK_CLIENT.post(
        '/webhook/',
        json.dumps(WEBHOOK_EVENT),
        content_type='application/json'
    ).status_code == 404


@mock.patch('django_gocardless.utils.is_valid_signature')
def test_webhook_no_resource(mock_sig: mock.MagicMock):
    mock_sig.return_value = True
    event = WEBHOOK_EVENT.copy()
    event['events'][0]['resource_type'] = 'test'
    assert WEBHOOK_CLIENT.post(
        '/webhook/',
        json.dumps(WEBHOOK_EVENT),
        content_type='application/json'
    ).status_code == 200
