"""
Routing for the go Cardless payments system.

This is by and large controlled by Django Rest Frameworks routing system.
"""
from django.conf.urls import url, include

from rest_framework import routers

from django_gocardless import views


mandate_router = routers.SimpleRouter()
mandate_router.register(r'mandates', views.GoCardlessMandateViewSet, base_name='mandate')

payments_router = routers.SimpleRouter()
payments_router.register(r'payments', views.GoCardlessPaymentViewSet, base_name='payments')

subscriptions_router = routers.SimpleRouter()
subscriptions_router.register(r'subscriptions', views.GoCardlessSubscriptionViewSet, base_name='subscriptions')

urlpatterns = [
    url('', include(mandate_router.urls)),
    url('', include(payments_router.urls)),
    url('', include(subscriptions_router.urls)),
    url('webhook/', views.GoCardlessWebhookView.as_view())
]
