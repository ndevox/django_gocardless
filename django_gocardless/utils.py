""" General utilities for GoCardless system. """
import hashlib
import hmac

import gocardless_pro
from django.conf import settings


def get_client() -> gocardless_pro.Client:
    """ Get a GoCardless client using the appropriate settings. """
    return gocardless_pro.Client(
        settings.GO_CARDLESS_KEY,
        environment='sandbox' if settings.DEBUG else 'live')


def is_valid_signature(body: bytes, provided_signature: str) -> bool:
    """ Compare a GoCardless signature against our own for validity. """
    secret = bytes(settings.GO_CARDLESS_WEBHOOK_SECRET, 'utf-8')
    computed_signature = hmac.new(
        secret, body, hashlib.sha256).hexdigest()
    return hmac.compare_digest(provided_signature, computed_signature)
