""" Views for the GoCardless system. """
import json
import logging

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.urls import reverse
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from django_gocardless import models, serializers, utils, forms


LOGGER = logging.getLogger(__name__)


class GoCardlessMandateViewSet(viewsets.GenericViewSet):
    """
    ViewSet for controlling GoCardless Mandates.
    """

    model = models.GoCardlessMandate

    serializer_class = serializers.GoCardlessMandateSerializer

    @transaction.atomic
    def create(self, request):
        """ Create a Direct Debit mandate for a user. """
        mandate = self.model.objects.get_or_create(user=request.user)[0]

        if mandate.active:
            return Response('Mandate already exists', status=409)

        client = utils.get_client()

        redirect_flow = client.redirect_flows.create(
            params={
                'description': '{}'.format(settings.GO_CARDLESS_DESCRIPTION),
                'session_token': mandate.session_token(),
                'success_redirect_url': request.build_absolute_uri(
                    reverse('mandate-success', args=[mandate.pk])
                ),
            }
        )

        mandate.redirect_flow_id = redirect_flow.id
        mandate.save()

        return Response({'url': redirect_flow.redirect_url})

    @action(detail=True, methods=['GET'])
    def success(self, request, pk):
        """ Finalise the creation of a Direct Debit mandate for a user. """
        mandate = models.GoCardlessMandate.objects.get(pk=pk)

        if mandate.redirect_flow_id != request.GET.get('redirect_flow_id'):
            return Response(status=400)

        client = utils.get_client()

        redirect_flow = client.redirect_flows.complete(
            mandate.redirect_flow_id,
            params={
                'session_token': mandate.session_token()
            }
        )

        mandate.mandate_id = redirect_flow.links.mandate
        mandate.active = True
        mandate.save()

        return Response({'url': redirect_flow.confirmation_url})


class BaseGoCardlessViewSet(viewsets.GenericViewSet,
                            mixins.RetrieveModelMixin,
                            mixins.ListModelMixin):
    """ Basic ViewSet for GoCardless subscription/payments views. """
    form = None
    model = None
    serializer_class = None

    def create(self, request):
        """ Create the relevant model using the data given. """
        data = request.data.copy()

        data['user'] = request.user.pk
        data['mandate'] = request.user.gocardlessmandate.pk

        form = self.form(data)

        if form.is_valid():
            form.save()
            return Response(status=201)

        return Response(form.errors.as_json(), status=400)


class GoCardlessPaymentViewSet(BaseGoCardlessViewSet):
    """ ViewSet for GoCardless Payments. """
    form = forms.GoCardlessPaymentForm
    model = models.GoCardlessPayment
    serializer_class = serializers.GoCardlessPaymentSerializer

    def get_queryset(self):
        """ Limit payments to the user. """
        return self.request.user.gocardlesspayment_set.all()


class GoCardlessSubscriptionViewSet(BaseGoCardlessViewSet):
    """ ViewSet for GoCardless Subscriptions. """
    form = forms.GoCardlessSubscriptionForm
    model = models.GoCardlessSubscription
    serializer_class = serializers.GoCardlessSubscriptionSerializer

    def get_queryset(self):
        """ Limit subscriptions to the user. """
        return self.request.user.gocardlesssubscription_set.all()


class GoCardlessWebhookView(APIView):
    """
    APIView to receive GoCardless web hooks.

    There can be a lot of different web hooks so we offload what we can to the models.

    We define what models we can offload to in RESOURCE_TO_MODEL.
    """
    authentication_classes = []

    RESOURCE_TO_MODEL = {
        'mandates': models.GoCardlessMandate,
        'payments': models.GoCardlessPayment,
        'subscriptions': models.GoCardlessSubscription
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.response = Response()

    def post(self, request):
        """
        Validate that the webhook is a valid webhook.

        If it is, process each event in the webhook.
        """
        if not utils.is_valid_signature(request.body, request.META['HTTP_WEBHOOK_SIGNATURE']):
            self.response.status_code = 498
            return self.response

        payload = json.loads(request.body.decode('utf-8'))

        for event in payload['events']:
            LOGGER.info('Received webhook event: %s', str(event))
            self.process(event)
        return self.response

    def process(self, event: dict):
        """
        Make sure this event uses a resource we know how to handle.

        If we know the resource, try to handle the action.

        If that fails because we don't know the action, log it and move on.

        If it fails because the relevant instance of the model does not exist,
        return a 404.
        """
        if not event['resource_type'] in self.RESOURCE_TO_MODEL:
            LOGGER.info('No hook response for resource %s.', event['resource_type'])
            return

        model = self.RESOURCE_TO_MODEL[event['resource_type']]
        try:
            model.process_event(event)
        except AttributeError:
            LOGGER.info('No hook response for action %s on %s.', event['action'], str(model))
        except ObjectDoesNotExist:
            LOGGER.error('No object for model %s with data %s.', str(model), str(event))
            self.response.status_code = 404
