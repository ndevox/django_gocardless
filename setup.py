from setuptools import setup

install_reqs = [req.strip() for req in open('requirements.txt').readlines()]

setup(
    name="django_gocardless",
    version="0.1.0",
    packages=["django_gocardless"],
    install_requires=install_reqs,
    url="https://gitlab.com/ndevox/django_gocardless"
)
